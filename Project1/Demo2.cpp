#include <conio.h> //getch
#include <iostream> //cin/cout

using namespace std;

int main() {

	//int input;
	//cout << "Enter an integer: ";
	//cin >> input;

	//if (input % 2) cout << input << " is odd.\n"; // anything nonzero in if statement resolves to true
	//else cout << input << " is even.\n";

	//int i;
	//cout << "enter 1-3";
	//cin >> i;

	//switch (i) {
	//case 3: cout << "1 squared is 1"; break; //will count down from case # if no breaks
	//case 2: cout << "2 squared is 4"; break;
	//case 1: cout << "3 squared is 9"; break;
	//default: cout << "not a valid number";
	//}


	// loops

	for (int i = 1; i <= 10; i++) { //any part inside for loop can be removed, still need ";"s
		cout << i << "\n";
	}

	int input;
	cout << "Enter an integer (1-5)";
	while (input <= 0 || input > 5) {
		cout << "really...";
		cin >> input;
	}
	cout << "Congrats you're not an idiot.";
	_getch();
	return 0;
}